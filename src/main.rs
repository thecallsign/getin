mod invoiced_model;
mod profile;
mod error;

use invoiced_model::InvoicedData;

use clap::{Arg, App, SubCommand, ArgMatches};

use std::io::{Read, Write};
use std::fs::File;
use crate::profile::TomlProfile;
use crate::error::{GetInErrorKind as ErrorKind, GetInError as Error, GetInError, GetInErrorKind};
use std::path::{Path, PathBuf};
use crate::invoiced_model::Item;
use curl::easy::{Easy, List, ReadError};

const ENV_CONFIG_KEY: &str = "GETIN_CONFIG_PATH";
const API_ENDPOINT: &str = "https://invoice-generator.com";

#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let app = App::new("getin")
        .version("0.1")
        .author("thecallsign <stjohn@thecallsign.net>")
        .about(r#"Quickly generate an invoice"#)
        .args_from_usage(r#"
            -c, --config=[CONFIG]    'Invoice config'"#)
        .subcommand(SubCommand::with_name("new")
            .about("Start building a new invoice")
            .args_from_usage(
                r#"--logo=[LOGO]      'Set the URL for the logo'
                    -f, --from=[FROM]     'The name of your organization'
                    -t, --to=[TO] 	    'The entity being billed - multiple lines ok'
                    --number=[NUMBER] 	'Invoice number '
                    --purchase_order=[ORDER] 	'Purchase order number '
                    -d, --date=[DATE] 	'Invoice date 	current date'
                    --payment_terms=[PAYMENT_TERMS] 	'Payment terms'
                    --due_date=[DUE] 	'Invoice due date '
                    --discounts=[DISCOUNT] 	'Subtotal discounts - numbers only'
                    --tax=[TAX] 	'Tax - numbers only'
                    --shipping=[SHIPPING] 	'Shipping - numbers only'
                    --amount_paid=[PAID] 	'Amount paid - numbers only'
                    --notes=[NOTES] 	'Notes - any extra information not included elsewhere '
                    --terms=[TERMS] 	'Terms and conditions - all the details '"
                    --currency=[CURRENCY]    'Invoice currency'"#
            )
        )
        .subcommand(SubCommand::with_name("item")
            .about("Add an item to the given invoice")
            .args_from_usage(r#"
                    -n, --name=<NAME> 'Item name'
                    -c, --unit_cost=<COST> 'Unit cost'
                    -q, --quantity=<QUANTITY> 'Number of this item'
                    -d, --description=[DESC] 'Description of the item'
                "#))
        .subcommand(SubCommand::with_name("build")
            .about("Build an invoice using a config")
            .visible_alias("gen")
            .args_from_usage(r#"
                    -o, --output=<OUTPUT>    'Output file name, should include the `.pdf` extension'
                "#))
        .subcommand(SubCommand::with_name("example")
            .about("Generate an example toml config file"));

    let matches = app.get_matches();

    let config_path = {
        let env_config = std::env::var_os(ENV_CONFIG_KEY);
        if env_config.is_none() {
            let s = matches.value_of("config").unwrap_or_else(|| {
                eprintln!("Failure to find config file path in either the env or the cmdline");
                eprintln!("Exiting.");
                println!("{}", matches.usage());
                std::process::exit(-1);
            });
            PathBuf::from(s)
        } else {
            let path = env_config
                .expect(&format!("Failed to parse env var {}", ENV_CONFIG_KEY));
            PathBuf::from(path)
        }
    };

    match matches.subcommand() {
        ("new", Some(sub_m)) => {
            create_new_config(&config_path, sub_m).unwrap();
            std::process::exit(0);
        }
        ("item", Some(sub_m)) => {
            let mut config = TomlProfile::load(&config_path)?;
            let item = Item {
                name: sub_m.value_of("name").expect("Invalid argument").to_owned(),
                quantity: sub_m.value_of("quantity").map(|s| s.parse().map_err(|_| Error::new(ErrorKind::InvalidArgument)).unwrap())
                    .expect("Unable to parse quantity"),
                unit_cost: sub_m.value_of("unit_cost").map( |s| s.parse().map_err(|_| Error::new(ErrorKind::InvalidArgument)).unwrap())
                    .expect("Unable to parse unit cost"),
                description: sub_m.value_of("description").map(|s| s.to_owned()),
            };

            if let Some(data) =  config.mut_data() {
                data.items.get_or_insert(vec![]).push(item);
            };

            config.save().unwrap();
            std::process::exit(0);
        }
        ("build", Some(sub_m)) => {
            build_invoice(&config_path, sub_m).unwrap();
            std::process::exit(0);
        },
        _ => {}
    }
    let config_file = matches.value_of("config");

    println!("{}", matches.usage());
    Ok(())
}

fn build_invoice(path: &Path, matches: &ArgMatches) -> Result<(), Error> {
    let config = TomlProfile::load(path)?;
    let invoice_data = config.data().as_ref().expect("Attempted to send invoice data without loading data first");

    let output_filename = matches.value_of("output")
        .expect("Output filename not present, this is a bug");
    let mut data = vec![];

    let mut handle = Easy::new();

    handle.url(API_ENDPOINT).expect("Invalid API URI");
    handle.post(true).expect("Failure setting POST headers");

    let mut data_to_transfer = config.json_data().expect("Failed to convert toml to json");
    let mut bytes_to_transfer = data_to_transfer.as_bytes();

    let mut headers = List::new();
    headers.append("Accept: application/json");
    headers.append("Content-Type: application/json");

    handle.post_field_size(bytes_to_transfer.len() as u64);
    handle.http_headers(headers).expect("Failure setting content-type headers");
    {
        let mut transfer = handle.transfer();
        transfer.read_function(|into| {
            use std::io::Read;
            Ok(bytes_to_transfer.read(into).unwrap_or_else(|_| 0))
        }).expect("Failed writing to stream");

        transfer.write_function(|new_data| {
            data.extend_from_slice(new_data);
            Ok(new_data.len())
        }).expect("Failed reading from stream");
        transfer.perform().expect("Failure communicating with server");
    }

    let mut file = File::create(output_filename).map_err(|err| {
        GetInError::new(GetInErrorKind::FileSystemError(err))
    })?;

    file.write(&data).map_err(|err| {
        GetInError::new(GetInErrorKind::FileSystemError(err))
    })?;

    Ok(())
}

fn create_new_config(path: &Path, matches: &ArgMatches) -> Result<(), Error> {
    let mut config = TomlProfile::new(path).map_err(|error| {
        match error.kind {
            ErrorKind::ConfigAlreadyExists => {
                eprintln!("Config file already exists, exiting.");
                std::process::exit(1);
            },
            _ => {
                eprintln!("Error while reading config: {}", error);
                std::process::exit(-2);
            }
        }
    })?;

    let data;

    data = InvoicedData {
        logo: matches.value_of("logo").map(|s| s.to_owned()),
        from: matches.value_of("from").map(|s| s.to_owned()),
        to: matches.value_of("to").map(|s| s.to_owned()),
        number: matches.value_of("number").map(|s| s.to_owned()),
        purchase_order: matches.value_of("order").map(|s| s.to_owned()),
        date: matches.value_of("date").map(|s| s.to_owned()),
        payment_terms: matches.value_of("payment_terms").map(|s| s.to_owned()),
        due_date: matches.value_of("due").map(|s| s.to_owned()),
        discounts: matches.value_of("discount").map(|s| s.parse().map_err(|_| Error::new(ErrorKind::InvalidArgument)).unwrap()),
        tax: matches.value_of("tax").map(|s| s.parse().map_err(|_| Error::new(ErrorKind::InvalidArgument)).unwrap()),
        shipping: matches.value_of("shipping").map(|s| s.parse().map_err(|_| Error::new(ErrorKind::InvalidArgument)).unwrap()),
        amount_paid: matches.value_of("paid").map(|s| s.parse().map_err(|_| Error::new(ErrorKind::InvalidArgument)).unwrap()),
        notes: matches.value_of("notes").map(|s| s.to_owned()),
        terms: matches.value_of("terms").map(|s| s.to_owned()),
        items: Some(vec![Item { name: "Software Developer".to_owned(), quantity: 40, unit_cost: 550.0, description: None }, Item { name: "Linux SysAdmin".to_owned(), quantity: 20, unit_cost: 450.0, description: None } ]),
        ..InvoicedData::default()
    };

    config.use_data(data);
    config.save().unwrap();
    Ok(())

}