use serde::{Serialize, Deserialize};


#[derive(Debug, Clone, Default,  Deserialize, Serialize)]
pub struct InvoicedData {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub currency: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub fields: Option<Vec<String>>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub header: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub to_title: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub invoice_number_title: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub date_title: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub payment_terms_title: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub due_date_title: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub purchase_order_title: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub quantity_header: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub item_header: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub unit_cost_header: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub amount_header: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub subtotal_title: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub discounts_title: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub tax_title: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub shipping_title: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub total_title: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub amount_paid_title: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub balance_title: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub terms_title: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub notes_title: Option<String>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub logo: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub from: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub to: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub number: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub purchase_order: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub date: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub payment_terms: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub due_date: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub discounts: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub tax: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub shipping: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub amount_paid: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub notes: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub terms: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub items: Option<Vec<Item>>,
}

#[derive(Debug, Clone, Default, Deserialize, Serialize)]
pub struct Item {
    pub name: String,
    pub quantity: i64,
    pub unit_cost: f64,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub description: Option<String>
}