use std::fs::{File, OpenOptions};
use crate::invoiced_model::InvoicedData;
use std::path::Path;
use crate::error::{GetInError, GetInErrorKind};
use crate::error::GetInErrorKind::{FileSystemError, ConfigAlreadyExists};
use std::io::{Read, Write};
use std::io;

#[derive(Debug)]
pub struct TomlProfile {
    config_file: File,
    invoice_data: Option<InvoicedData>
}

impl TomlProfile {
    pub fn new(path: &Path) -> Result<TomlProfile, GetInError> {
        let mut file = OpenOptions::new()
            .create_new(true)
            .write(true)
            .open(path)
            .map_err(|err| {
                match err.kind() {
                    std::io::ErrorKind::AlreadyExists => {
                        dbg!(&path);
                        dbg!(&err);
                        dbg!(&err.kind());
                        GetInError::new(ConfigAlreadyExists)
                    },
                    _ => {
                        GetInError::new(FileSystemError(err))
                    }
                }
            })?;

        let profile = TomlProfile {
            config_file: file,
            invoice_data: None
        };

        Ok(profile)
    }

    pub fn use_data(&mut self, data: InvoicedData) {
        self.invoice_data = Some(data);
    }

    pub fn data(&self) -> &Option<InvoicedData> {
        &self.invoice_data
    }

    pub fn mut_data(&mut self) -> &mut Option<InvoicedData> {
        &mut self.invoice_data
    }

    pub fn save(&mut self) -> Result<(), GetInError> {
        let buffer = toml::to_vec(
            &self.invoice_data.as_ref().expect("Writing to disk while InvoicedData not set, this is a bug")
            ).map_err(|err| {
                GetInError::new(GetInErrorKind::TOMLSerializationError(err))
            })?;

        self.config_file.write_all(&buffer)
            .map_err(|err| {
                GetInError::new(GetInErrorKind::FileSystemError(err))
            })?;

        Ok(())
    }

    pub fn load(path: &Path) -> Result<TomlProfile, GetInError> {
        let mut file = OpenOptions::new()
            .read(true)
            .write(true)
            .open(path)
            .map_err(|err| {
                GetInError::new(FileSystemError(err))
            })?;

        let mut buffer = Vec::with_capacity(128);
        file.read_to_end(&mut buffer).map_err(|err| {
            GetInError::new(GetInErrorKind::FileSystemError(err))
        });

        let data = toml::from_slice::<InvoicedData>(&buffer)
            .map_err(|err| {
                GetInError::new(GetInErrorKind::TOMLParseError(err))
            })?;

        Ok(TomlProfile {
            config_file: file,
            invoice_data: Some(data)
        })
    }

    pub fn json_data(&self) -> Result<String, GetInError> {
        serde_json::to_string(
            self.invoice_data
                .as_ref()
                .expect("Building invoice without invoice data, this is a bug")
        ).map_err(|err| {
            GetInError::new(GetInErrorKind::JSONSerializationError(err))
        })
    }
}