use std::error::Error;
use std::fmt::{Display, Formatter, Pointer};
use std::fmt;

#[derive(Debug)]
pub struct GetInError {
    pub kind: GetInErrorKind,
    pub description: Option<String>
}

impl GetInError {
    pub fn new(kind: GetInErrorKind) -> GetInError {
        GetInError {
            kind: kind,
            description: None
        }
    }

    pub fn description(mut self, description: String) -> GetInError {
        self.description = Some(description);
        self
    }
}

impl Display for GetInError {
    fn fmt(&self, f: &mut Formatter) -> Result<(), std::fmt::Error> {
        write!(f, "GetInError: {:?}\nDescription: {:?}", self.kind, self.description)
    }
}

impl Error for GetInError {

}

#[derive(Debug)]
pub enum GetInErrorKind {
    ConfigAlreadyExists,
    InvalidArgument,
    JSONSerializationError(serde_json::Error),
    ByteParseError(std::io::Error),
    TOMLSerializationError(toml::ser::Error),
    FileSystemError(std::io::Error),
    TOMLParseError(toml::de::Error)
}
