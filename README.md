# GetIn - A command line invoice generator

GetIn uses a stateful TOML configuration to save your invoices and templates.
All commands require an argument (or environment variable) that is a path to a configuration file.

The following is a small example invoice configuration file:
```toml
from = "thecallsign"
to = "Stranger Labs, Inc"

[[items]]
name = "Software Developer"
quantity = 40
unit_cost = 550.0

[[items]]
name = "Linux SysAdmin"
quantity = 20
unit_cost = 450.0
```

GetIn supports all the configuration keys that invoice-generator.com has stated in their API docs.
You may view the supported keys here: https://github.com/Invoiced/invoice-generator-api

## Usage

Create a configuration file `demo.toml` and populate it with a few initial values

```bash
getin -c demo.toml new -f thecallsign -t "Stranger Labs, Inc"
```

Add some items to the config
```bash
getin -c demo.toml item -n "Software Developer" -q 40 -c 550
getin -c demo.toml item -n "Linux SysAdmin" -q 20 -c 450
```

Build a pdf of the invoice

```bash
getin -c demo.toml build -o output.pdf
```

Please note that this is a work in progress, and a few of the command line subcommands and features have not yet been implemented.
